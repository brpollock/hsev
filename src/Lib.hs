module Lib
    ( someFunc
    ) where


-- both outcomes from a starting value v
coinToss v = [v * 1.5, v * 0.6]

-- all outcomes for the list of starting values after one round
coinTosses [] = []
coinTosses (v:vs) = coinToss v ++ coinTosses vs

-- all outcomes for every round
gameRounds' vs = vs : gameRounds' (coinTosses vs)
gameRounds = gameRounds' [1]


-- divide a list of values between bins of size (binsize 1..)
-- TODO: should be binmax?
binify' _ [] _ = []
binify' binsize vals bindex = (top, inbin) : binify' binsize outbin (bindex+1)
    where
        top    = binsize bindex
        inbin  = filter (<  top) vals
        outbin = filter (>= top) vals
binify binsize vals = binify' binsize vals 1

-- histogram of 'binified' data
histogram fwidth bins = foldl (++) "" (map (format . count) bins)
    where
        format (bin, cnt) = rpad bin 7 ++ " " ++ stars (fwidth cnt) ++ "\n"
        count (bin, vals) = (bin, length vals)
        stars n  = take n (repeat '*')
        rpad x n = show x ++ take (n - length (show x)) (repeat ' ')

someFunc :: IO ()
someFunc = do
    let results = gameRounds !! 8
    let bins = binify binsize results
    putStrLn (histogram fwidth bins)
    where
        binsize n = 0.25 * 2 ^ (n - 1)
        fwidth n = ceiling (fromIntegral n / 2)
