# hsev

Haskell implementation of the expected value problem.

More info [here in the Python version](https://bitbucket.org/brpollock/expected-value-problem/src/master/).

The code is in [src/Lib.hs](src/Lib.hs).

To see the output:

 1. click [Pipelines](https://bitbucket.org/brpollock/hsev/addon/pipelines/home)
 2. select the most-recent successful build
 3. in the `Build` column click `stack exec -- hseve-exe` to expand it